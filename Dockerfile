FROM php:7.0-apache
ADD php.ini /usr/local/etc/php
ADD test.php /var/www/html
ADD info.php /var/www/html
RUN apt-get update && apt-get install -y \
        nano \
  mlocate \
  libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng12-dev \
        unixodbc unixodbc-dev unixodbc-bin libodbc1 odbcinst1debian2 tdsodbc \
        freetds-bin freetds-common freetds-dev libct4 libsybdb5 \
    && docker-php-ext-install -j$(nproc) iconv mcrypt pdo_mysql opcache \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-configure pdo_odbc --with-pdo-odbc=unixODBC,/usr \
    && docker-php-ext-install -j$(nproc) pdo_odbc
RUN pecl install sqlsrv  \
    && docker-php-ext-enable sqlsrv 
RUN pecl install pdo_sqlsrv  \
    && docker-php-ext-enable pdo_sqlsrv 

COPY opcache.ini /tmp/
COPY odbc.ini /tmp/
COPY odbcinst.ini /tmp/
COPY freetds.conf /tmp/
RUN cat /tmp/opcache.ini >> /usr/local/etc/php/php.ini  && rm -f /tmp/opcache.ini
RUN cat /tmp/odbc.ini >> /etc/odbc.ini  && rm -f /tmp/odbc.ini
RUN cat /tmp/odbcinst.ini >> /etc/odbcinst.ini  && rm -f /tmp/odbcinst.ini
RUN cat /tmp/freetds.conf >> /etc/freetds/freetds.conf  && rm -f /tmp/freetds.conf
RUN echo "extension= pdo_sqlsrv.so" >> `php --ini | grep "Loaded Configuration" | sed -e "s|.*:\s*||"`
RUN echo "extension= sqlsrv.so" >> `php --ini | grep "Loaded Configuration" | sed -e "s|.*:\s*||"`
RUN echo "export TERM=xterm" >> ~/.bashrc
RUN a2enmod rewrite

